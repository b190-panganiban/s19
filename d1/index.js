//console.log("Hello, B190!");

//if statement
/*
	-executes a statement if a specified condition is true
	-can stand alom without the else statement

	Syntax:
		if(condition) {
			statement/code block
		}

*/

let numA = -1;

if(numA < 0) {
	console.log("Hello!");
}

console.log(numA < 0)

if(numA > 0) {
	console.log("This statement will not be printed!")
}


let city = "New York"

if(city === "New York") {
	console.log("Welcome to New York City!");
}

//else if clause
/*
	-executes a statement if previous conditions are false and if the specified  condition is true
	- "else if" clause is optional and can be added to capture additional conditions to change the flow of a program

*/

let numB = 1;

if (numA > 0) {
	console.log("Hello!")
} else if (numB > 0) {
	console.log("World");
}

if (numA < 0) {
	console.log("Hello!")
} else if (numB > 0) {
	console.log("World");
}

city = "Tokyo";

if(city === "New York") {
	console.log("Welcome to New York City!");
} else if (city === "Tokyo") {
	console.log("Welcome to Tokyo!");
}

//else statement
/*
	-executes a statement if all other conditions are false
	-the "else" statement is optional and can be added to capture any other result to change the flow of a program

*/

if (numA > 0) {
	console.log("Hello");
} else if (numB === 0) {
	console.log("World");
} else {
	console.log("Again");
}

//Another Example
// let age = prompt("Enter your age:")

// if (age <= 18) {
// 	console.log("Not allowed to drink!")
// } else {
// 	console.log("Matanda ka na, shot na!")
// }

/*
	Mini Activity: 6:32PM
		Create a conditional statement if the height is below 150, display "Did not passed the minimum height requirement". If above or equal 150, display "Passed the minimum height requirement."
	
	**stretch goal:
		put it inside a function

*/

//solution 1
let height = 160;

if (height < 150) {
	console.log("Did not passed!")
} else {
	console.log("Passed the min. height requirement.")
}


//solution 2
function heightReq(h) {

	if(h < 150) {
		console.log("Did not passed the min. height requirement")
	} else {
		console.log("Passed the minimum height requirement.")
	}

}

heightReq(140);
heightReq(150);
heightReq(160);
//heightReq("Renz");

let message = "No message.";
console.log(message)

function determineTyphoonIntensity(windSpeed) {

	if (windSpeed < 30) {
		return 'Not a typhoon yet.'
	} else if (windSpeed <= 61) {
		return 'Tropical depression detected.'
	} else if (windSpeed >= 62 && windSpeed <= 88) {
		return 'Tropical storm detected.'
	} else if (windSpeed >= 89 && windSpeed <= 117) {
		return 'Severe tropical storm detected.'
	} else {
		return 'Typhoon detected.'
	}

}

message = determineTyphoonIntensity(69);
console.log(message);

if (message == "Tropical storm detected.") {
	console.warn(message)
}


//Truthy and Falsy
/*
	-In JavaScript, a truthy value is a value that is considered true when encountered in a Boolean context
	-Values are considered true unless defined otherwise
	Falsy values/exceptions for truthy
	1. false
	2. 0
	3. -0
	4. ""
	5. null
	6. undefined
	7. NaN
*/

//Truthy Examples
if(true) {
	console.log("Truthy!");
}

if(1) {
	console.log("Truthy!");
}

if([]) {
	console.log("Truthy");
}

//Falsy Examples
if (false) {
	console.log("Falsy!");
}

if (0) {
	console.log("Falsy!");
}

if(undefined) {
	console.log("Falsy!");
}

//Conditional (Ternary) Operator
/*
	The Ternary operator takes in three operands
	1. condition
	2. expression to execute if the condition is truthy
	2. experssion to execute if the condition is falsy

	Syntax:
		(condition) ? ifTrue : ifFalse

*/

//Single Statement execution
let ternaryResult = (1 < 18) ? "Statement is True" : "Statement is False";
console.log("Result of Ternary Operator: " + ternaryResult)

let name;

// function isOfLegalAge() {
// 	name = 'John';
// 	return 'You are of the legal age limit';
// }

// function isUnderAge() {
// 	name = 'Jane';
// 	return 'You are under the age limit';
// }

// let age = parseInt(prompt("What is your age?"));
// console.log(age);
// console.log(typeof age)
// let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();
// console.log("Result of Ternary Operator in functions: " +legalAge + ', ' + name);


//Switch Statement
/*
	Syntax:
		switch (expression) {
			case value : 
				statement
				break;
			default:
				statement;
				break;
		}

*/

let day = prompt("What day of the week is it today?").toLowerCase();
console.log(day)

	switch (day) {
		case 'monday':
			console.log("The color of the day is red");
			break;
		case 'tuesday':
			console.log("The color of the day is orange");
			break;
		case 'wednesday':
			console.log("The color of the day is yellow");
			break;
		case 'thursday':
			console.log("The color of the day is green");
			break;
		case 'friday':
			console.log("The color of the day is blue");
			break;
		case 'saturday':
			console.log("The color of the day is indigo");
			break;
		case 'sunday':
			console.log("The color of the day is violet");
			break;
		// case ('saturday' || 'sunday'):
		// 	console.log("The color of the day is indigo");
		// 	break;
		default:
			console.log("Please input a valid day!");
			break;
	}

//Try-Catch-Finally Statement
function showIntensityAlert(windSpeed) {
	try {
		//Attempt to execute a code
		alerat(determineTyphoonIntensity(windSpeed))
	}

	catch (error) {

		//catch errors within "try" statement
		console.log(error)
		console.warn(error.message)
	}

	finally {
		//Continue execution of code regardless of success or failure of code execution in the "try" block to handle/resolve errors
		alert("Intesity updates will show new alert.")
	}
}
showIntensityAlert(56);